import React from "react";
import {
  UserOutlined,
  LogoutOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import styled from "styled-components";
import { Divider } from "antd";

const MenuItem = styled.li`
  width: 200px;
  display: flex;
  align-items: center;
  cursor: pointer;
  background-color: white;
  font-size: 17px;
  font-weight: 500;
  padding: 5px 10px;
  &:hover {
    background-color: lightblue;
  }
`;

export default function UserMenu() {
  return (
    <ul className="text-black pt-2">
      <MenuItem>
        <UserOutlined />
        <span className="ml-3">View Profile</span>
      </MenuItem>
      <MenuItem>
        <SettingOutlined />
        <span className="ml-3">Setting</span>
      </MenuItem>
      <Divider style={{ margin: "6px 0" }} />
      <MenuItem>
        <LogoutOutlined />
        <span className="ml-3">Log Out</span>
      </MenuItem>
      {/* <li>
        <UserOutlined />
        View Profile
      </li>
      <li>Setting</li> */}
    </ul>
  );
}
