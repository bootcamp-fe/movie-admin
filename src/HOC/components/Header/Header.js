import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import clsx from "clsx";
import styles from "./Header.module.scss";
import logo from "~/assets/image/logo.png";
import banner from "~/assets/image/banner.jpg";
import { localServ } from "~/services/localService";
import { Button, message, Tooltip } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faUser,
  faArrowRightFromBracket,
} from "@fortawesome/free-solid-svg-icons";
import { useDispatch } from "react-redux";
import UserMenu from "./components/UserMenu";

export default function Header() {
  const navigate = useNavigate();
  let dispatch = useDispatch();

  let UserInfo = localServ.user.get();

  const handleLogout = () => {
    localServ.user.remove();
    message.success("Loged out");
    setTimeout(() => {
      navigate("/login");
    }, 500);
  };

  return (
    <div className={clsx(styles.wrapper)}>
      <div className="mx-auto flex items-center justify-between h-full px-10">
        <a
          style={{
            height: "100%",
            display: "flex",
            alignItems: "center",
          }}
          href="/"
        >
          <img className={clsx("h-2/3 block w-100")} src={banner} />
        </a>

        <div className={clsx(styles.userInfo, "flex items-center space-x-5")}>
          <Tooltip
            overlayStyle={{
              borderRadius: 1,
              backgroundColor: "transparent",
              padding: "16px 1px 1px 1px",
              cursor: "default",
            }}
            placement="bottom"
            color={"#FFFFFF"}
            title={<UserMenu />}
          >
            <span
              style={{ cursor: "default" }}
              className="text-lg font-medium mr-2"
            >
              <span className="text-lg font-light">Xin chào</span>{" "}
              {UserInfo.hoTen}
            </span>
            <FontAwesomeIcon color="rgb(59, 91, 152)" size="2x" icon={faUser} />
          </Tooltip>
          <button
            onClick={handleLogout}
            className="px-3 py-2 bg-blue-500 font-medium rounded text-white"
          >
            <span className="mr-2">Log out</span>
            <FontAwesomeIcon size="lg" icon={faArrowRightFromBracket} />
          </button>
        </div>
      </div>
    </div>
  );
}
