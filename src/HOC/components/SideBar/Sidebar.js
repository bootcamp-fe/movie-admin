import React from "react";
import styles from "./Sidebar.module.scss";
import clsx from "clsx";
import SideMenu from "./SideMenu";

export default function Sidebar() {
  return (
    <div className={clsx(styles.content)}>
      <div className="font-medium py-2 pl-2">Dashboard</div>
      <SideMenu />
    </div>
  );
}
