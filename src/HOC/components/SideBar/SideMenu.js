import {
  UserOutlined,
  YoutubeOutlined,
  UnorderedListOutlined,
  VideoCameraAddOutlined,
  UserAddOutlined,
} from "@ant-design/icons";
import { Button, Menu } from "antd";
import React, { useEffect, useState } from "react";
import { Navigate, useLocation, useNavigate } from "react-router-dom";

function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}

const items = [
  getItem("Quan ly User", "1", <UserOutlined />),
  getItem("Quan ly Movie", "2", <YoutubeOutlined />),
  getItem("Quan ly admin", "3", <UserAddOutlined />),
];

const SideMenu = () => {
  const navigate = useNavigate();
  const [collapsed, setCollapsed] = useState(false);
  const location = useLocation();

  const toggleCollapsed = () => {
    setCollapsed(!collapsed);
  };

  const navigateKey = {
    1: "/usermanagement",
    2: "/movie-management",
    3: "/admin-management",
  };

  let getKeyByValue = (obj, val) =>
    Object.keys(obj).find((key) => obj[key] === val);

  const onClick = (e) => {
    let index = e.key;
    navigate(navigateKey[index]);
  };

  let handleCollapsed = () => {
    if (window.innerWidth >= 1200) {
      setCollapsed(false);
    } else {
      setCollapsed(true);
    }
  };
  let currentKey = getKeyByValue(navigateKey, location.pathname);
  useEffect(() => {
    if (window.innerWidth <= 1200) {
      setCollapsed(true);
    }
    window.addEventListener("resize", handleCollapsed);
    return () => window.removeEventListener("resize", handleCollapsed);
  }, []);

  return (
    <div>
      <Menu
        onClick={onClick}
        defaultSelectedKeys={currentKey}
        defaultOpenKeys={["sub1"]}
        mode="inline"
        theme="light"
        inlineCollapsed={collapsed}
        items={items}
      />
    </div>
  );
};

export default SideMenu;
