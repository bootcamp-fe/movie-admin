import React from "react";
import styles from "./Footer.module.scss";

export default function Footer() {
  return (
    <div className={styles.footer}>
      <p
        className="text-center mb-0 bg-transparent"
        style={{ color: "#848587" }}
      >
        &#169; 2022 Make by DinhHoang <span className="text-red-500">👀</span>
      </p>
    </div>
  );
}
