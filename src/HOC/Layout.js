import React from "react";
// import Header from "./components/Header/Header"
import {
  UploadOutlined,
  UserOutlined,
  VideoCameraOutlined,
} from "@ant-design/icons";
import Header from "./components/Header/Header";
import Sidebar from "./components/SideBar/Sidebar";
import clsx from "clsx";
import styles from "./Layout.module.scss";
import Footer from "./components/Footer/Footer";

export default function Layout({ Component }) {
  return (
    <>
      <Header />
      <div className={clsx(styles.contentWrapper)}>
        <Sidebar />
        <div className="flex flex-col flex-1 ml-1">
          <div className="flex-1 overflow-auto p-5 bg-slate-100 ">
            <Component />
            <Footer />
          </div>
        </div>
      </div>
    </>
  );
}
