import React, { useEffect, useLayoutEffect } from "react";
import { localServ } from "../services/localService";

export default function SecureView({ children }) {
  let userLocal = localServ.user.get();

  useLayoutEffect(() => {
    if (!userLocal) {
      window.location.href = "/login";
    }
  }, []);
  return <>{userLocal ? children : ""}</>;
}
