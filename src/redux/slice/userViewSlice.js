const { createSlice } = require("@reduxjs/toolkit");

const initState = {
  openAddUserModal: false,
};

const addUserSlice = createSlice({
  name: "addUserSlice",
  initialState: initState,
  reducers: {
    openAddUserModal: (state, action) => {
      state.openAddUserModal = action.payload;
    },
  },
});

export const { openAddUserModal } = addUserSlice.actions;

export default addUserSlice.reducer;
