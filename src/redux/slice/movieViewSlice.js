import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  editModal: { open: false, thongTinPhim: {} },
  addModal: { open: false },
  showtimeModal: { open: false, thongTimPhim: {} },
};

const movieViewSlice = createSlice({
  name: "movieViewSlice",
  initialState,
  reducers: {
    openEditModal: (state, action) => {
      state.editModal = action.payload;
    },
    openAddModal: (state, action) => {
      state.addModal = action.payload;
    },
    openShowtimeModal: (state, action) => {
      state.showtimeModal = action.payload;
    },
  },
});

export const { openEditModal, openAddModal, openShowtimeModal } =
  movieViewSlice.actions;

export default movieViewSlice.reducer;
