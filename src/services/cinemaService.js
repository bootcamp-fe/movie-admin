import axios from "axios";
import { BASE_URL, https, TOKEN_CYBERSOFT } from "./configURL";
import { localServ } from "./localService";

export const cinemaServ = {
  getCinemaList: () => {
    let uri = `/api/QuanLyRap/LayThongTinHeThongRap`;
    return axios({
      url: `${BASE_URL}${uri}`,
      method: "GET",
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: "Bearer " + localServ.user.get()?.accessToken,
      },
      timeout: 10000,
    });
  },
  getCinemaName: (name) => {
    let uri = `/api/QuanLyRap/LayThongTinCumRapTheoHeThong?maHeThongRap=${name}`;
    return axios({
      url: `${BASE_URL}${uri}`,
      method: "GET",
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: "Bearer " + localServ.user.get()?.accessToken,
      },
      timeout: 10000,
    });
  },
  setShowtime: (data) => {
    let uri = `/api/QuanLyDatVe/TaoLichChieu`;
    return axios({
      url: `${BASE_URL}${uri}`,
      method: "POST",
      data,
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: "Bearer " + localServ.user.get()?.accessToken,
      },
      timeout: 10000,
    });
  },
};
