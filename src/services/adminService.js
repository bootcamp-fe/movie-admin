import axios from "axios";
import { SMS_URL } from "./configURL";

export const adminServ = {
  getAdminList: () => {
    return axios({
      url: `${SMS_URL}/admins`,
      method: "GET",
      timeout: "10000",
    });
  },
};
