import axios from "axios";
import { BASE_URL, https, TOKEN_CYBERSOFT } from "./configURL";
import { localServ } from "./localService";

export const moviesServ = {
  getListMovie: () => {
    let uri = `/api/QuanLyPhim/LayDanhSachPhim?maNhom=${
      localServ.user.get()?.maNhom
    }`;
    return axios({
      url: `${BASE_URL}${uri}`,
      method: "GET",
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: "Bearer " + localServ.user.get()?.accessToken,
      },
      timeout: 10000,
    });
  },
  getMovieByTheater: () => {
    let uri = `/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=${
      localServ.user.get()?.maNhom
    }`;
    return axios({
      url: `${BASE_URL}${uri}`,
      method: "GET",
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: "Bearer " + localServ.user.get()?.accessToken,
      },
      timeout: 10000,
    });
  },
  getMovieInfo: (maPhim) => {
    let uri = `/api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`;
    return axios({
      url: `${BASE_URL}${uri}`,
      method: "GET",
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: "Bearer " + localServ.user.get()?.accessToken,
      },
      timeout: 10000,
    });
  },
  updateMovieInfo: (data) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/CapNhatPhimUpload`,
      method: "POST",
      data,
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: "Bearer " + localServ.user.get()?.accessToken,
      },
      timeout: 10000,
    });
  },
  uploadNewMovie: (data) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/ThemPhimUploadHinh`,
      method: "POST",
      data,
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: "Bearer " + localServ.user.get()?.accessToken,
      },
      timeout: 10000,
    });
  },
  deleteMovie: (maPhim) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/XoaPhim?MaPhim=${maPhim}`,
      method: "DELETE",
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: "Bearer " + localServ.user.get()?.accessToken,
      },
      timeout: 10000,
    });
  },
};
