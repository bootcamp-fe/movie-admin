import "./App.css";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import LoginPage from "./pages/LoginPage/LoginPage";
import "antd/dist/antd.min.css";
import Layout from "./HOC/Layout";
import SecureView from "./HOC/SecureView";
import MovieManagement from "./pages/MovieManagement/MovieManagement";
import Usermanagement from "./pages/UserManagement/UserManagement";
import SignupPage from "./pages/SignupPage/SignupPage";
import AdminManagement from "./pages/AdminManagement/AdminManagement";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Navigate to="/usermanagement" />} />
        <Route path="/login" element={<LoginPage />} />
        <Route path="/sign-up" element={<SignupPage />} />
        <Route
          path="/usermanagement"
          element={
            <SecureView>
              <Layout Component={Usermanagement} />
            </SecureView>
          }
        />
        <Route
          path="/movie-management"
          element={
            <SecureView>
              <Layout Component={MovieManagement} />
            </SecureView>
          }
        />
        <Route
          path="/admin-management"
          element={
            <SecureView>
              <Layout Component={AdminManagement} />
            </SecureView>
          }
        />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
