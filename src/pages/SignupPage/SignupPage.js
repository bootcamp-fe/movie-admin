import { Button, Checkbox, Divider, Form, Input, message, theme } from "antd";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import Lottie from "lottie-react";
import movie_animate from "../../assets/lf20_cbrbre30.json";
import { userServ } from "../../services/userService";
import {
  UserOutlined,
  LockOutlined,
  SmileOutlined,
  MailOutlined,
  PhoneOutlined,
} from "@ant-design/icons";
import bgImg from "~/assets/image/login-bg.png";
import logoImg from "~/assets/image/logo.png";
import { QUAN_TRI_VIEN } from "~/services/localService";

const SignupPage = () => {
  const [form] = Form.useForm();
  let navigate = useNavigate();
  let [loading, setLoading] = useState(false);

  const onFinish = (fieldsValue) => {
    const values = {
      ...fieldsValue,
      maLoaiNguoiDung: QUAN_TRI_VIEN,
      maNhom: "GP01",
    };
    setLoading(true);
    userServ
      .signUpNewUser(values)
      .then((res) => {
        setLoading(false);
        form.resetFields();
        message.success("Đăng ký thành công!");
        setTimeout(() => {
          navigate("/login");
        }, 1000);
      })
      .catch((err) => {
        setLoading(false);
        console.log(err);
        message.error(err.response.data.content || "Đăng ký không thành công!");
      });
  };

  const onFinishFailed = (errorInfo) => {
    setLoading(false);
    console.log("Failed:", errorInfo);
  };

  return (
    <div style={{ backgroundImage: `url(${bgImg})` }} className="">
      <div className="container mx-auto h-screen w-screen flex items-center justify-center ">
        <div
          style={{
            boxShadow: " rgba(0, 0, 0, 0.24) 0px 3px 8px",
            minHeight: "700px",
          }}
          className="w-3/4 flex rounded-lg "
        >
          <div
            style={{ backgroundColor: "rgba(4, 86, 250, 0.7" }}
            className="w-1/2 rounded-lg flex items-center justify-center"
          >
            <div className="w-3/4">
              <Lottie animationData={movie_animate} />
            </div>
          </div>
          <div className="w-1/2 flex flex-col items-center justify-center ">
            <img
              className="animate-pulse"
              style={{ height: "100px" }}
              src={logoImg}
              alt=""
            />
            <p className="text-3xl font-medium">Sign Up</p>
            <Form
              form={form}
              className=" w-4/5"
              layout="vertical"
              name="basic"
              wrapperCol={{
                span: 24,
              }}
              initialValues={{}}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                name="taiKhoan"
                rules={[
                  {
                    whitespace: true,
                    required: true,
                    message: "Hãy nhập vào tài khoản",
                  },
                  {
                    pattern: /^\S*$/,
                    message: "Tài khoản không được chứa khoảng trắng!",
                  },
                ]}
              >
                <Input
                  style={{
                    borderRadius: "5px",
                    height: "40px",
                  }}
                  prefix={<UserOutlined className="site-form-item-icon mr-2" />}
                  placeholder="Username"
                />
              </Form.Item>
              <Form.Item
                name="matKhau"
                rules={[
                  {
                    whitespace: true,
                    required: true,
                    message: "Hãy nhập vào mật khẩu",
                  },
                  {
                    pattern: /^\S*$/,
                    message: "Mật khẩu không được chứa khoảng trắng!",
                  },
                ]}
              >
                <Input
                  style={{
                    borderRadius: "5px",
                    height: "40px",
                  }}
                  prefix={<LockOutlined className="site-form-item-icon mr-2" />}
                  type="password"
                  placeholder="Password"
                />
              </Form.Item>
              <Form.Item
                name="hoTen"
                rules={[
                  {
                    whitespace: true,
                    required: true,
                    message: "Hãy nhập vào họ tên",
                  },
                ]}
              >
                <Input
                  style={{
                    borderRadius: "5px",
                    height: "40px",
                  }}
                  prefix={
                    <SmileOutlined className="site-form-item-icon mr-2" />
                  }
                  placeholder="Họ và tên"
                />
              </Form.Item>
              <Form.Item
                name="email"
                rules={[
                  {
                    required: true,
                    message: "Hãy nhập vào email!",
                  },
                  {
                    type: "email",
                    message: "Định dạng email không hợp lệ",
                  },
                ]}
              >
                <Input
                  style={{
                    borderRadius: "5px",
                    height: "40px",
                  }}
                  prefix={<MailOutlined className="site-form-item-icon mr-2" />}
                  placeholder="Email"
                />
              </Form.Item>
              <Form.Item name="soDt">
                <Input
                  style={{
                    borderRadius: "5px",
                    height: "40px",
                  }}
                  prefix={
                    <PhoneOutlined className="site-form-item-icon mr-2" />
                  }
                  placeholder="Số điện thoại"
                />
              </Form.Item>
              <Form.Item
                wrapperCol={{
                  span: 24,
                }}
              >
                <Button
                  style={{
                    backgroundColor: "#365BBD",
                    borderRadius: "5px",
                    borderColor: "transparent",
                    color: "white",
                    display: "block",
                    height: "40px",
                    fontSize: "16px",
                    fontWeight: "500",
                  }}
                  className="w-full mt-5 "
                  type="ghost"
                  htmlType="submit"
                  loading={loading}
                >
                  Sign up
                </Button>
              </Form.Item>
            </Form>
            <div className="flex justify-end w-3/4">
              <p>Already have an account?</p>
              <span
                onClick={() => {
                  navigate("/login");
                }}
                className="ml-3 text-blue-500 cursor-pointer"
              >
                Log in
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignupPage;

// email
// :
// "admintest123@gmail.com"
// hoTen
// :
// "Admin Test"
// maLoaiNguoiDung
// :
// "QuanTri"
// maNhom
// :
// "GP01"
// matKhau
// :
// "admintest"
// soDt
// :
// "0123456789"
// taiKhoan:"admintest"
