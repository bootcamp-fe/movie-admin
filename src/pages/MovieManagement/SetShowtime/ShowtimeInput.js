import {
  Button,
  DatePicker,
  Form,
  Input,
  InputNumber,
  message,
  Select,
  Switch,
  Tooltip,
  Upload,
} from "antd";
import moment from "moment";
import TextArea from "antd/lib/input/TextArea";
import { UploadOutlined, PlusOutlined } from "@ant-design/icons";
import { moviesServ } from "~/services/moviesService";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { cinemaServ } from "~/services/cinemaService";
import { Option } from "antd/lib/mentions";

export default function ShowtimeInput({ movieInfo, closeModal }) {
  const [form] = Form.useForm();
  let customForm = Form.useForm();
  let [loading, setLoading] = useState(false);
  let [cinemaState, setCinemaState] = useState({
    heThongRap: [],
    tenRap: [],
  });
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  console.log(movieInfo);
  // Get cinema List
  useEffect(() => {
    cinemaServ
      .getCinemaList()
      .then((res) => {
        setCinemaState((state) => ({ ...state, heThongRap: res.data.content }));
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  // Get cinema name by chage cinema
  let handleChangeCinema = (e) => {
    form.resetFields(["maRap"]);
    cinemaServ
      .getCinemaName(e)
      .then((res) => {
        setCinemaState((state) => {
          let newState = { ...state };
          newState.tenRap = res.data.content;
          return newState;
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const onFinish = (fieldsValue) => {
    const values = {
      ...fieldsValue,
      ngayChieuGioChieu: moment(fieldsValue["ngayChieuGioChieu"]).format(
        "DD/MM/YYYY HH:MM:SS"
      ),
    };
    cinemaServ
      .setShowtime(values)
      .then((res) => {
        message.success(res.data.content || "Thêm lịch chiếu thành công!");
        closeModal();
      })
      .catch((err) => {
        console.log(err);
        message.error(
          err.response.data.content || "Thêm lịch chiếu không thành công!"
        );
      });
  };
  return (
    <div>
      <div className="flex items-end">
        <img className="w-1/4 rounded-md " src={movieInfo.hinhAnh} alt="" />
        <div className="pl-5 font-bold">
          <p>Mã phim {movieInfo.maPhim}</p>
          <p className="mb-0">Tên phim {movieInfo.tenPhim}</p>
        </div>
      </div>
      <div className="mt-10">
        <Form
          form={form}
          name="basic"
          labelCol={{
            span: 7,
          }}
          labelAlign={"left"}
          wrapperCol={{
            span: 17,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          onAbortCapture={(e) => {
            console.log(e);
          }}
        >
          <Form.Item
            style={{ display: "none" }}
            initialValue={movieInfo.maPhim.toString()}
            name="maPhim"
          >
            <Input />
          </Form.Item>
          <Form.Item
            rules={[{ required: true, message: "Hãy chọn cụm rạp!" }]}
            label="Cụm rạp"
          >
            <Select onChange={handleChangeCinema} placeholder="Chọn cụm rạp">
              {cinemaState.heThongRap?.map((item) => {
                return (
                  <Select.Option key={item.biDanh} value={item.maHeThongRap}>
                    {item.tenHeThongRap}
                  </Select.Option>
                );
              })}
            </Select>
          </Form.Item>
          <Form.Item
            rules={[{ required: true, message: "Hãy chọn rạp!" }]}
            label="Rạp"
            name="maRap"
          >
            <Select
              placeholder="Chọn rạp"
              value={cinemaState.tenRap[0]?.tenCumRap}
              defaultActiveFirstOption={true}
            >
              {cinemaState.tenRap?.map((item) => {
                return (
                  <Select.Option key={item.maCumRap} value={item.maCumRap}>
                    {item.tenCumRap}
                  </Select.Option>
                );
              })}
            </Select>
          </Form.Item>

          <Form.Item
            rules={[{ required: true, message: "Chọn ngày khởi chiếu!" }]}
            label="Ngày khởi chiếu"
            name="ngayChieuGioChieu"
          >
            <DatePicker
              placeholder="Chọn ngày"
              showTime
              format="DD-MM-YYYY HH:mm:ss"
            />
          </Form.Item>

          <Form.Item>
            <Tooltip
              color="#FAFAFA"
              trigger={["focus"]}
              title={
                <p className="px-3 mb-0 text-black">
                  Giá vé từ 75 - 100 nghìn!
                </p>
              }
              placement="bottom"
              overlayClassName="numeric-input"
            >
              <Form.Item
                labelCol={{
                  span: 10,
                }}
                labelAlign={"left"}
                wrapperCol={{
                  span: 14,
                }}
                label="Giá vé"
                name="giaVe"
                rules={[{ required: true, message: "Nhập vào giá vé!" }]}
              >
                <InputNumber
                  style={{ width: "100%" }}
                  placeholder="Nhập giá vé"
                  min={75000}
                  max={150000}
                />
              </Form.Item>
            </Tooltip>
          </Form.Item>
          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <div className="flex justify-end ">
              <Button
                loading={loading}
                className="end"
                type="primary"
                htmlType="submit"
              >
                Thêm
              </Button>
            </div>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
