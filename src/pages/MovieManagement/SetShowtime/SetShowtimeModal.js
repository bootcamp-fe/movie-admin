import { Modal } from "antd";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { openShowtimeModal } from "~/redux/slice/movieViewSlice";
import ShowtimeInput from "./ShowtimeInput";

export default function SetShowtimeModal({ fetchMovieList }) {
  const modalState = useSelector((state) => {
    return state.movieViewSlice.showtimeModal;
  });
  let movieInfo = modalState.thongTinPhim;
  let dispatch = useDispatch();
  const handleCancel = () => {
    dispatch(openShowtimeModal({ open: false, thongTinPhim: {} }));
  };
  return (
    <Modal
      title={
        <div className="flex items-center justify-between w-2/3">
          <p className="mb-0">Thêm lịch chiếu phim</p>
        </div>
      }
      open={modalState.open}
      onCancel={handleCancel}
      footer={null}
      destroyOnClose={true}
    >
      <ShowtimeInput closeModal={handleCancel} movieInfo={movieInfo} />
    </Modal>
  );
}
