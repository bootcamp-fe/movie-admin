import { Modal } from "antd";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { openAddModal } from "~/redux/slice/movieViewSlice";
import NewMovieInput from "./NewMovieInput";

export default function AddMovieModal({ fetchMovieList }) {
  const modalState = useSelector((state) => {
    return state.movieViewSlice.addModal;
  });
  let dispatch = useDispatch();
  const handleCancel = () => {
    dispatch(openAddModal({ open: false }));
  };
  return (
    <Modal
      title={
        <div className="flex items-center justify-center">
          <p className="mb-0 text-lg">Thêm phim</p>
        </div>
      }
      open={modalState.open}
      onCancel={handleCancel}
      footer={null}
      destroyOnClose={true}
      width={540}
      style={{
        padding: 8,
        borderRadius: 8,
        backgroundColor: "white",
      }}
    >
      <NewMovieInput
        closeModal={handleCancel}
        fetchMovieList={fetchMovieList}
      />
    </Modal>
  );
}
