import {
  Button,
  DatePicker,
  Form,
  Input,
  message,
  Rate,
  Switch,
  Upload,
} from "antd";
import moment from "moment";
import TextArea from "antd/lib/input/TextArea";
import { UploadOutlined, PlusOutlined } from "@ant-design/icons";
import { moviesServ } from "~/services/moviesService";
import { useState } from "react";
import { localServ } from "~/services/localService";

const getFile = (e) => {
  let fileSize = e.file.size;
  let fileType = e.file.type;

  let _1Mb = 1048576;
  const allowedFileTypes = ["image/png", "image/jpeg", "image/gif"];

  const isImg = allowedFileTypes.includes(fileType);
  const isLt1Mb = fileSize < _1Mb;

  if (!isImg || !isLt1Mb) {
    message.error("Vui lòng chọn file hình ảnh nhỏ hơn 1MB!");
    return undefined;
  }
  if (Array.isArray(e)) {
    return e;
  }
  return e && e.fileList;
};

export default function NewMovieInput({ closeModal, fetchMovieList }) {
  let [loading, setLoading] = useState(false);
  let maNhom = localServ.user.get().maNhom;
  const fileList = [];
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const onFinish = (fieldsValue) => {
    setLoading(true);
    let formData = new FormData();

    const values = {
      ...fieldsValue,
      ngayKhoiChieu: moment(fieldsValue["ngayKhoiChieu"]).format("DD-MM-YYYY"),
      moTa: fieldsValue.moTa.target.value,
    };

    for (const key in values) {
      if (key != "hinhAnh") {
        formData.append(key, values[key]);
      } else if (key == "hinhAnh") {
        if (values.hinhAnh === undefined) {
          formData.append("hinhAnh", null);
        } else {
          formData.append(
            "File",
            values.hinhAnh[0].originFileObj,
            values.hinhAnh[0].name
          );
        }
      }
    }
    moviesServ
      .uploadNewMovie(formData)
      .then((res) => {
        setLoading(false);
        message.success(res.data.message);
        fetchMovieList();
        setTimeout(() => {
          closeModal();
        }, 1000);
      })
      .catch((err) => {
        setLoading(false);
        message.error(err.message || "Thêm phim không thành công!");
      });
  };
  return (
    <div>
      <Form
        name="basic"
        labelCol={{
          span: 6,
          offset: 2,
        }}
        labelAlign={"left"}
        wrapperCol={{
          span: 16,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item initialValue={maNhom} hidden label="Mã Nhóm" name="maNhom">
          <Input />
        </Form.Item>

        <Form.Item
          rules={[{ required: true, message: "Nhập vào tên phim" }]}
          label="Tên Phim"
          name="tenPhim"
        >
          <Input />
        </Form.Item>
        <Form.Item initialValue={""} label="Trailer" name="trailer">
          <Input />
        </Form.Item>
        <Form.Item
          initialValue={""}
          valuePropName="text"
          label="Mô tả"
          name="moTa"
        >
          <TextArea rows={4} />
        </Form.Item>
        <Form.Item
          rules={[{ required: true, message: "Chọn ngày khởi chiếu!" }]}
          label="Ngày khởi chiếu"
          name="ngayKhoiChieu"
        >
          <DatePicker format="DD-MM-YYYY" />
        </Form.Item>
        <Form.Item
          initialValue={false}
          valuePropName="checked"
          name="dangChieu"
          label="Đang chiếu"
        >
          <Switch />
        </Form.Item>
        <Form.Item
          initialValue={false}
          valuePropName="checked"
          name="sapChieu"
          label="Sắp chiếu"
        >
          <Switch />
        </Form.Item>
        <Form.Item
          initialValue={false}
          valuePropName="checked"
          name="hot"
          label="Hot"
        >
          <Switch />
        </Form.Item>
        <Form.Item
          rules={[{ required: true, message: "Hãy chọn hình ảnh" }]}
          name="hinhAnh"
          getValueFromEvent={getFile}
          label="Ảnh bìa"
          valuePropName="fileList"
        >
          <Upload
            maxCount={1}
            beforeUpload={(e) => false}
            listType="picture"
            className="upload-list-inline"
            onRemove={() => {
              return undefined;
            }}
          >
            <Button icon={<UploadOutlined />}>Thêm hình</Button>
          </Upload>
        </Form.Item>
        <Form.Item initialValue={0} label="Số sao" name="danhGia">
          <Rate count={10} />
        </Form.Item>
        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <div className="flex justify-end ">
            <Button
              loading={loading}
              className="end"
              type="primary"
              htmlType="submit"
              style={{ padding: "0 24px" }}
            >
              Thêm
            </Button>
          </div>
        </Form.Item>
      </Form>
    </div>
  );
}
