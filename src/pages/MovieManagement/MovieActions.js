import { Button, message } from "antd";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { openEditModal, openShowtimeModal } from "~/redux/slice/movieViewSlice";
import { moviesServ } from "~/services/moviesService";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCalendar,
  faPenToSquare,
  faTrash,
} from "@fortawesome/free-solid-svg-icons";

export default function MovieActions({ maPhim, refreshMovieList }) {
  let dispatch = useDispatch();
  let [editBtnLoading, setEditBtnLoading] = useState(false);
  let [delBtnLoading, setDelBtnLoading] = useState(false);
  let [showtimeBtnLoading, setShowtimeBtnLoading] = useState(false);
  let openModal = () => {
    setEditBtnLoading(true);
    moviesServ
      .getMovieInfo(maPhim)
      .then((res) => {
        let thongTinPhim = res.data.content;
        dispatch(openEditModal({ open: true, thongTinPhim }));
        setEditBtnLoading(false);
      })
      .catch((err) => {
        setEditBtnLoading(false);

        console.log(err);
      });
  };
  let deleteMovie = () => {
    setDelBtnLoading(true);
    moviesServ
      .deleteMovie(maPhim)
      .then((res) => {
        refreshMovieList();
        message.success("Xóa thành công!");
        setDelBtnLoading(false);
        console.log(res);
      })
      .catch((err) => {
        message.error("Xóa không thành công");
        setDelBtnLoading(false);
        console.log(err);
      });
  };
  let openShowtime = () => {
    setShowtimeBtnLoading(true);
    moviesServ
      .getMovieInfo(maPhim)
      .then((res) => {
        setShowtimeBtnLoading(false);
        let thongTinPhim = res.data.content;
        dispatch(openShowtimeModal({ open: true, thongTinPhim }));
      })
      .catch((err) => {
        setShowtimeBtnLoading(false);
        console.log(err);
      });
  };
  return (
    <div className="space-x-2">
      <Button
        onClick={deleteMovie}
        loading={delBtnLoading}
        type="danger"
        className="px-5 py-1 rounded bg-red-400 text-white font-medium"
        icon={<FontAwesomeIcon icon={faTrash} />}
      >
        <div className=""></div>
      </Button>
      <Button
        onClick={openModal}
        loading={editBtnLoading}
        icon={<FontAwesomeIcon icon={faPenToSquare} />}
        type="primary"
      >
        <div className=""></div>
      </Button>
      <Button
        onClick={openShowtime}
        loading={showtimeBtnLoading}
        icon={
          <div>
            <FontAwesomeIcon icon={faCalendar} />
          </div>
        }
        style={{ background: "#5EBA7D", color: "white" }}
      >
        <div className=""></div>
      </Button>
    </div>
  );
}
