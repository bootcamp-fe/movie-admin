import {
  Button,
  DatePicker,
  Form,
  Input,
  InputNumber,
  message,
  Switch,
  Upload,
} from "antd";
import moment from "moment";
import TextArea from "antd/lib/input/TextArea";
import { UploadOutlined, PlusOutlined } from "@ant-design/icons";
import { moviesServ } from "~/services/moviesService";
import { useState } from "react";

const getFile = (e) => {
  if (e.fileList.length == 0) {
    return undefined;
  }
  if (Array.isArray(e)) {
    return e;
  }
  return e && e.fileList;
};

export default function MovieEditForm({
  movieInfo,
  handleCancel,
  fetchMovieList,
}) {
  let ngayKhoiChieu = moment(movieInfo.ngayKhoiChieu);
  let [loading, setLoading] = useState(false);
  const fileList = [
    {
      uid: "-1",
      name: movieInfo.tenPhim,
      status: "done",
      url: movieInfo.hinhAnh,
      thumbUrl: movieInfo.hinhAnh,
    },
  ];
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const onFinish = (values) => {
    setLoading(true);
    let formData = new FormData();
    values.ngayKhoiChieu = moment(values.ngayKhoiChieu).format("DD-MM-YYYY");
    for (const key in values) {
      if (key != "hinhAnh") {
        formData.append(key, values[key]);
      } else if (key == "hinhAnh") {
        if (values.hinhAnh === undefined) {
          formData.append("hinhAnh", null);
        } else {
          formData.append(
            "File",
            values.hinhAnh[0].originFileObj,
            values.hinhAnh[0].name
          );
        }
      }
    }
    moviesServ
      .updateMovieInfo(formData)
      .then((res) => {
        setLoading(false);
        message.success(res.data.message);
        fetchMovieList();
        setTimeout(() => {
          handleCancel();
        }, 1000);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
        message.error(
          err.response?.data.content ||
            err.message ||
            "Cật nhập không thành công!"
        );
      });
  };
  return (
    <div>
      <Form
        name="basic"
        labelCol={{
          span: 6,
          offset: 2,
        }}
        labelAlign={"left"}
        wrapperCol={{
          span: 16,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        onAbortCapture={(e) => {
          console.log(e);
        }}
      >
        <Form.Item
          hidden
          initialValue={movieInfo.maNhom}
          label="Mã Nhóm"
          name="maNhom"
        >
          <Input />
        </Form.Item>
        <Form.Item
          hidden
          initialValue={movieInfo.maPhim}
          label="Mã Phim"
          name="maPhim"
        >
          <Input />
        </Form.Item>

        <Form.Item
          initialValue={movieInfo.tenPhim}
          label="Tên Phim"
          name="tenPhim"
        >
          <Input />
        </Form.Item>
        <Form.Item
          initialValue={movieInfo.trailer}
          label="Trailer"
          name="trailer"
        >
          <Input />
        </Form.Item>
        <Form.Item initialValue={movieInfo.moTa} label="Mô tả" name="moTa">
          <TextArea rows={4} />
        </Form.Item>
        <Form.Item
          initialValue={ngayKhoiChieu}
          label="Ngày khởi chiếu"
          name="ngayKhoiChieu"
        >
          <DatePicker format="DD-MM-YYYY" />
        </Form.Item>
        <Form.Item
          initialValue={movieInfo.dangChieu}
          valuePropName="checked"
          name="dangChieu"
          label="Đang chiếu"
        >
          <Switch defaultChecked={movieInfo.dangChieu} />
        </Form.Item>
        <Form.Item
          initialValue={movieInfo.sapChieu}
          valuePropName="checked"
          name="sapChieu"
          label="Sắp chiếu"
        >
          <Switch defaultChecked={movieInfo.sapChieu} />
        </Form.Item>
        <Form.Item
          initialValue={movieInfo.hot}
          valuePropName="checked"
          name="hot"
          label="Hot"
        >
          <Switch defaultChecked={movieInfo.hot} />
        </Form.Item>
        <Form.Item
          name="hinhAnh"
          getValueFromEvent={getFile}
          label="Hình ảnh"
          valuePropName="fileList"
        >
          <Upload
            maxCount={1}
            beforeUpload={() => false}
            listType="picture"
            defaultFileList={[...fileList]}
            className="upload-list-inline"
            onRemove={() => {
              return undefined;
            }}
          >
            <Button icon={<UploadOutlined />}>Upload</Button>
          </Upload>
        </Form.Item>
        <Form.Item
          initialValue={movieInfo.danhGia}
          label="Số sao"
          name="danhGia"
        >
          <InputNumber min={1} max={10} />
        </Form.Item>
        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <div className="flex justify-end ">
            <Button
              loading={loading}
              className="end"
              type="primary"
              htmlType="submit"
            >
              Cật nhật
            </Button>
          </div>
        </Form.Item>
      </Form>
    </div>
  );
}
