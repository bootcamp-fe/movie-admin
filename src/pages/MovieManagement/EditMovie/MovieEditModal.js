import { Modal } from "antd";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { openEditModal } from "~/redux/slice/movieViewSlice";
import MovieEditForm from "./MovieEditForm";

export default function MovieEditModal({ fetchMovieList }) {
  const modalState = useSelector((state) => {
    return state.movieViewSlice.editModal;
  });
  let movieInfo = modalState.thongTinPhim;
  let dispatch = useDispatch();
  const handleCancel = () => {
    dispatch(openEditModal({ open: false, thongTinPhim: {} }));
  };
  return (
    <Modal
      title={
        <div className="flex items-center justify-between w-2/3">
          <p>Sửa thông tin phim</p>
        </div>
      }
      open={modalState.open}
      // confirmLoading={confirmLoading}
      onCancel={handleCancel}
      footer={null}
      destroyOnClose={true}
    >
      <MovieEditForm
        handleCancel={handleCancel}
        fetchMovieList={fetchMovieList}
        movieInfo={movieInfo}
      />
    </Modal>
  );
}
