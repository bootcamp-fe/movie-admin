import { Table } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { moviesServ } from "~/services/moviesService";
import { movieListHeader } from "./utitls.movieManagement";
import { openAddModal } from "~/redux/slice/movieViewSlice";

export default function MovieListTable({ movieList, loading, fetchMovieList }) {
  useEffect(() => {
    fetchMovieList();
  }, []);
  let dispatch = useDispatch();
  return (
    <Table
      title={() => (
        <div>
          <span className="text-md text-base">Danh sách phim</span>
          <button
            style={{ backgroundColor: "#3B82F6" }}
            onClick={() => {
              dispatch(openAddModal({ open: true }));
            }}
            className="ml-3 px-3 py-2 rounded font-medium text-white"
          >
            Thêm phim
          </button>
        </div>
      )}
      rowKey={(record) => record.maPhim}
      pagination={{
        position: ["bottomCenter"],
      }}
      columns={movieListHeader}
      dataSource={movieList}
      loading={loading}
    />
  );
}
