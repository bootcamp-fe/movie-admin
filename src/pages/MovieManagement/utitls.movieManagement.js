import { Rate, Tag } from "antd";
import { StarFilled } from "@ant-design/icons";

export const movieListHeader = [
  {
    title: "Mã phim",
    dataIndex: "maPhim",
    key: "maphim",
    width: "10%",
    className: "w-fit",
  },
  {
    title: "Hình ảnh",
    dataIndex: "hinhAnh",
    key: "hinhanh",
    render: (url) => (
      <img className="rounded-sm" style={{ width: "70px" }} src={url} alt="" />
    ),
    width: "10%",
    responsive: ["xxl", "xl", "lg", "md"],
  },
  {
    title: "Tên phim",
    dataIndex: "tenPhim",
    key: "tenphim",
    width: "15%",
  },
  {
    title: "Mô tả",
    dataIndex: "moTa",
    key: "mota",
    width: "20%",
    ellipsis: true,
    responsive: ["xxl", "xl", "lg", "md"],
  },
  {
    title: "Đánh giá",
    dataIndex: "danhGia",
    key: "danhgia",
    render: (startNum) => {
      return (
        <Rate
          character={<StarFilled style={{ fontSize: "1rem" }} />}
          disabled
          className="text-sm"
          allowHalf
          value={startNum / 2}
        />
      );
    },
    responsive: ["xl"],
    width: "20%",
  },
  {
    title: "Hành động",
    dataIndex: "action",
    key: "action",
    width: "20%",
  },
];

//"maPhim": 10374,
//"tenPhim": "Uncharged",
//"biDanh": "uncharged",
//"trailer": "https://www.youtube.com/watch?v=eHp3MbsCbMg",
//"hinhAnh": "https://movienew.cybersoft.edu.vn/hinhanh/uncharged_gp01.jpg",
//"moTa": "ádfjsaldkfsdf",
//"maNhom": "GP01",
//"ngayKhoiChieu": "2022-10-03T00:00:00",
//"danhGia": 8,
//"hot": true,
//"dangChieu": true,
//"sapChieu": true
