import React, { useState } from "react";
import MovieEditModal from "./EditMovie/MovieEditModal";
import MovieListTable from "./MovieListTable";
import MovieActions from "./MovieActions";
import { moviesServ } from "~/services/moviesService";
import AddMovieModal from "./AddNewMovie/AddMovieModal";
import SetShowtimeModal from "./SetShowtime/SetShowtimeModal";

export default function MovieManagement() {
  const [movieList, setMovieList] = useState([]);
  const [loading, setLoading] = useState(false);
  let fetchMovieList = () => {
    setLoading(true);
    moviesServ
      .getListMovie()
      .then((res) => {
        let data = res.data.content.map((item) => {
          return {
            ...item,
            action: (
              <MovieActions
                refreshMovieList={fetchMovieList}
                maPhim={item.maPhim}
              />
            ),
          };
        });
        setMovieList(data);
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
      });
  };
  return (
    <div className=" mx-auto  bg-white rounded-lg">
      <SetShowtimeModal />
      <MovieEditModal fetchMovieList={fetchMovieList} />
      <AddMovieModal fetchMovieList={fetchMovieList} />
      <MovieListTable
        movieList={movieList}
        loading={loading}
        fetchMovieList={fetchMovieList}
      />
    </div>
  );
}
