import { Button, Modal } from "antd";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import UserInputForm from "./UserInputForm";
import { openAddUserModal } from "~/redux/slice/userViewSlice";
import { localServ } from "~/services/localService";
import NewUserInputForm from "./NewUserInputForm";

const UserAddModal = ({ modalState }) => {
  const [confirmLoading, setConfirmLoading] = useState(false);
  let [userInputInfo, setUserInfo] = useState({});
  let dispatch = useDispatch();
  let maNhom = localServ.user.get()?.maNhom;
  const handleOk = () => {
    setConfirmLoading(true);
    setTimeout(() => {
      setConfirmLoading(false);
    }, 2000);
  };

  const handleCancel = () => {
    dispatch(openAddUserModal(false));
  };

  return (
    <>
      <Modal
        title={
          <div className="flex items-center justify-between w-2/3">
            <p>Thêm người dùng</p>
          </div>
        }
        open={modalState}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        destroyOnClose={true}
        footer={null}
      >
        <NewUserInputForm closeModal={handleCancel} />
      </Modal>
    </>
  );
};

export default UserAddModal;
