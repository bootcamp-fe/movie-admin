import { Button, Divider, message } from "antd";
import React, { useEffect, useState } from "react";
import { localServ } from "~/services/localService";
import { controller, userServ } from "../../services/userService";
import UserActions from "./UserActions";
import UserEditModal from "./UserEditModal";
import UserTable from "./UserTable";
import { openAddUserModal } from "~/redux/slice/userViewSlice";
import { useDispatch, useSelector } from "react-redux";
import UserAddModal from "./UserAddModal";
import { UserAddOutlined } from "@ant-design/icons";
import { adminServ } from "~/services/adminService";

const modalInitState = { open: false, data: {} };

export default function AdminManagement() {
  const [adminList, setAdminList] = useState([]);
  let [modalState, setModalState] = useState(modalInitState);
  let addUserModalState = useSelector(
    (state) => state.userViewSlice.openAddUserModal
  );
  let [loading, setLoading] = useState(false);
  let dispatch = useDispatch();
  let openModal = (userInfo) => {
    setModalState({ open: true, data: userInfo });
  };

  let closeModal = () => {
    setModalState(false);
  };
  let fetchAdminList = () => {
    setLoading(true);
    adminServ
      .getAdminList()
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });
  };
  useEffect(() => {
    console.log("use Effect");
    fetchAdminList();
  }, []);
  return <div className=" mx-auto  bg-white rounded-lg"></div>;
}
