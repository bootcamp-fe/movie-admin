import { Button, Divider, message } from "antd";
import React, { useEffect, useState } from "react";
import { localServ } from "~/services/localService";
import { controller, userServ } from "../../services/userService";
import UserActions from "./UserActions";
import UserEditModal from "./UserEditModal";
import UserTable from "./UserTable";
import { openAddUserModal } from "~/redux/slice/userViewSlice";
import { useDispatch, useSelector } from "react-redux";
import UserAddModal from "./UserAddModal";
import { UserAddOutlined } from "@ant-design/icons";

const modalInitState = { open: false, data: {} };

export default function Usermanagement() {
  const [userList, setUserList] = useState([]);
  let [modalState, setModalState] = useState(modalInitState);
  let addUserModalState = useSelector(
    (state) => state.userViewSlice.openAddUserModal
  );
  let [loading, setLoading] = useState(false);
  let dispatch = useDispatch();
  let openModal = (userInfo) => {
    setModalState({ open: true, data: userInfo });
  };

  let closeModal = () => {
    setModalState(false);
  };
  let fetchUserList = () => {
    setLoading(true);
    userServ
      .getUserList()
      .then((res) => {
        let taiKhoanAdmin = localServ.user.get().taiKhoan;
        let filterAdminData = res.data.content.filter(
          (x) => x.taiKhoan !== taiKhoanAdmin
        );
        let data = filterAdminData.map((item, index) => {
          return {
            ...item,
            action: (
              <UserActions
                buttonIndex={index}
                openModal={openModal}
                onSuccess={fetchUserList}
                taiKhoan={item.taiKhoan}
              />
            ),
          };
        });
        setUserList(data);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });
  };
  useEffect(() => {
    fetchUserList();
  }, []);
  return (
    <div className=" mx-auto  bg-white rounded-lg">
      <UserEditModal
        fetchUserList={fetchUserList}
        modalState={modalState}
        closeModal={closeModal}
      />
      <UserAddModal modalState={addUserModalState} />
      <div className="px-3 py-4 font-normal text-base flex items-center">
        <p className="mb-0 text-md">Danh sách User</p>
        <button
          onClick={() => {
            dispatch(openAddUserModal(true));
          }}
          type="primary"
          className="ml-5 py-1 px-3 bg-blue-500 rounded font-medium text-white"
        >
          Thêm User
        </button>
      </div>
      <div className="px-1">
        <UserTable userList={userList} loading={loading} />
      </div>
    </div>
  );
}
