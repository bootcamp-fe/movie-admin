import {
  Button,
  Checkbox,
  Divider,
  Form,
  Input,
  message,
  Select,
  Typography,
} from "antd";
import React, { useEffect, useState } from "react";
import { userServ } from "~/services/userService";

const UserInputForm = ({ userInfo, closeModal, fetchUserList }) => {
  const [loading, setLoading] = useState(false);
  const onFinish = (values) => {
    setLoading(true);
    userServ
      .updateUserInfo(values)
      .then((res) => {
        setLoading(false);
        message.success("Cật nhật thành công!");
        fetchUserList();
        setTimeout(closeModal, 500);
      })
      .catch((err) => {
        setLoading(false);
        message.error(err.response.data.content);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="">
      <Form
        disabled={true}
        name="basic"
        labelCol={{
          span: 6,
          offset: 2,
        }}
        labelAlign={"left"}
        wrapperCol={{
          span: 16,
        }}
        autoComplete="off"
      >
        <Form.Item
          initialValue={userInfo.taiKhoan}
          label="Tài khoản"
          name="taiKhoan"
        >
          <Input />
        </Form.Item>
      </Form>
      <Form
        name="basic"
        labelCol={{
          span: 6,
          offset: 2,
        }}
        labelAlign={"left"}
        wrapperCol={{
          span: 16,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          hidden
          initialValue={userInfo.taiKhoan}
          label="Tài Khoản"
          name="taiKhoan"
        >
          <Input />
        </Form.Item>
        <Form.Item
          hidden
          initialValue={userInfo.maNhom}
          label="Nhóm"
          name="maNhom"
        >
          <Input />
        </Form.Item>
        <Form.Item
          initialValue={userInfo.matKhau}
          label="Mật Khẩu"
          name="matKhau"
          rules={[
            {
              required: true,
              message: "Hãy nhập vào mật khẩu",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          initialValue={userInfo.hoTen}
          label="Họ Tên"
          name="hoTen"
          rules={[
            {
              required: true,
              message: "Hãy nhập vào họ tên!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          initialValue={userInfo.email}
          label="Email"
          name="email"
          rules={[
            {
              required: true,
              message: "Hãy nhập vào email!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          initialValue={userInfo.soDT}
          label="Số Điện Thoại"
          name="soDt"
          rules={[
            {
              required: true,
              message: "Hãy nhập vào Số Điện Thoại!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          initialValue={userInfo.maLoaiNguoiDung}
          rules={[
            {
              required: true,
              message: "Hãy nhập vào mã người dùng!",
            },
          ]}
          label="Mã Người Dùng"
          name="maLoaiNguoiDung"
        >
          <Select>
            <Select.Option value="QuanTri">Quan Tri</Select.Option>
            <Select.Option value="KhachHang">Khach Hang</Select.Option>
          </Select>
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <div className="flex justify-end ">
            <Button
              loading={loading}
              className="end"
              type="primary"
              htmlType="submit"
            >
              Cật nhật
            </Button>
          </div>
        </Form.Item>
      </Form>
    </div>
  );
};

export default UserInputForm;

// {
//     "taiKhoan": "250196",
//     "matKhau": "aaaaa",
//     "email": "faker@gmail.com",
//     "soDt": "9876543210",
//     "maNhom": "GP01",
//     "maLoaiNguoiDung": "QuanTri",
//     "hoTen": "Admin"
//     }
