import { Button, Form, Input, message, Select } from "antd";
import React, { useEffect } from "react";
import { localServ } from "~/services/localService";
import { userServ } from "~/services/userService";

const NewUserInputForm = ({ closeModal }) => {
  const [form] = Form.useForm();
  const onFinish = (values) => {
    userServ
      .addUser(values)
      .then((res) => {
        message.success("Thêm user thành công!");
        form.resetFields();
        setTimeout(() => {
          closeModal();
        }, 1000);
      })
      .catch((err) => {
        console.log(err);
        message.error(err.response.data.content);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  useEffect(() => {}, []);

  let maNhom = localServ.user.get()?.maNhom;

  return (
    <div className="flex justify-center mx-auto">
      <div style={{ width: 480 }} className="bg-white px-5 rounded-lg">
        <Form
          form={form}
          labelCol={{
            span: 7,
          }}
          labelAlign={"left"}
          wrapperCol={{
            span: 16,
            offset: 1,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item hidden initialValue={maNhom} label="Mã nhóm" name="maNhom">
            <Input />
          </Form.Item>
          <Form.Item
            label="Tài Khoản"
            name="taiKhoan"
            rules={[
              {
                whitespace: true,
                required: true,
                message: "Hãy nhập vào tài khoản",
              },
              {
                pattern: /^\S*$/,
                message: "Tài khoản không được chứa khoảng trắng!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Mật Khẩu"
            name="matKhau"
            rules={[
              {
                whitespace: true,
                required: true,
                message: "Hãy nhập vào mật khẩu",
              },
              {
                pattern: /^\S*$/,
                message: "Mật khẩu không được chứa khoảng trắng!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            label="Họ Tên"
            name="hoTen"
            rules={[
              {
                required: true,
                message: "Hãy nhập vào họ tên!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Email"
            name="email"
            rules={[
              {
                required: true,
                message: "Hãy nhập vào email!",
              },
              {
                type: "email",
                message: "Định dạng email không hợp lệ",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Số Điện Thoại"
            name="soDt"
            rules={[
              {
                required: true,
                message: "Hãy nhập vào Số Điện Thoại!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            rules={[
              {
                required: true,
                message: "Hãy nhập vào mã người dùng!",
              },
            ]}
            label="Mã Người Dùng"
            name="maLoaiNguoiDung"
          >
            <Select>
              <Select.Option value="QuanTri">Quan Tri</Select.Option>
              <Select.Option value="KhachHang">Khach Hang</Select.Option>
            </Select>
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <div className="flex justify-end ">
              <Button className="end" type="primary" htmlType="submit">
                Thêm
              </Button>
            </div>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default NewUserInputForm;
